//I2C_TOP 2022.6
//该程序作为I2C协议中的
module I2C_TOP(
start_en,				//键值为0时使能系统		 
single_W,		
single_write_number,	
serial_write_number	
single_R,       
//I2C不需要识别读/写状态，其读写状态根据主/从机的时序信号自行判断。
clk,			     //主时钟频率
I2C_SCL,			 //时钟信号,周期为clk_double的一半，其中clk_double由主时钟提供，SCL为其分频
I2C_SDA,			 //数据信号
busy_mark,     //此状态0为忙，此时不接受外部控制
sda_out
);

input					start_en;
input					single_W;
input					single_write_number;
input					serial_write_number;
input					single_R;
input					clk;
inout					I2C_SDA;
output				I2C_SCL;
output				busy_mark;
output				sda_out;
output				busy_mark;



reg	[7:0]     switch_number;
reg	[127:0]		serial_write_number_keep;
wire					HZ_en;
wire					clk_double;



localparam		YES=1'd0;
localparam		NO=	1'd1;	



assign				I2C_SDA=(HZ_en==0)?sda_out:1'bz;	//三态门，当HZ_en为0时，I2C_SDA端口作输出口对从设备输出；当HZ_en为1时，I2C_SDA端口作输入端口获取从机输入

divide #(.WIDTH(4),.N(11)) u1
(.clk_in(clk),
 .clk_out(I2C_SCL)
);

divide #(.WIDTH(4),.N(11)) u2
 (.clk_in(clk),
 .clk_out(clk_double),
);

single_write  u3
(.start_en(start_en),
 .number(single_write_number),
 .single_W(single_W),
 .I2C_SCL(I2C_SCL),
 .I2C_SDA(I2C_SDA),
 .clk_double(clk_double),
 .HZ_en(HZ_en)
 .busy_mark(busy_mark)
);

serial_write  u4
(.start_en(start_en),
 .count(count),
 .number(serial_write_number_keep),
 .single_W(single_W),
 .I2C_SCL(I2C_SCL),
 .I2C_SDA(I2C_SDA),
 .clk_double(clk_double),
 .HZ_en(HZ_en),
 .busy_mark(busy_mark)
);

single_read  u5
(.start_en(start_en),
 .single_R(single_W),
 .I2C_SCL(I2C_SCL),
 .I2C_SDA(I2C_SDA),
 .clk_double(clk_double),
 .HZ_en(HZ_en)
 .busy_mark(busy_mark)
);

serial_read  u6
(.start_en(start_en),
 .count(count),
 .single_R(single_W),
 .I2C_SCL(I2C_SCL),
 .I2C_SDA(I2C_SDA),
 .clk_double(clk_double),
 .HZ_en(HZ_en),
 .busy_mark(busy_mark)
);

endmodule