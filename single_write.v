module single_write	//该设备向外设备进行写操作
(start_en,
 single_W,
 number,
 I2C_SCL,
 I2C_SDA,
 clk_double,
 HZ_en,
 busy_mark
);
 input					start_en;
 input					single_w;
 input[31:0]		number;
 input					I2C_SCL;
 inout					I2C_SDA;
 input					clk_double;
 output					HZ_en;
 output					busy_mark;
 
localparam			YES=1'd0;
localparam			NO=	1'd1;	
 
reg							HZ_en_in;
reg[3:0]				state;
reg							sda_out;
reg[7:0]				dev_addr;
reg[7:0]				reg_addr_H;
reg[7:0]				reg_addr_L;
reg[7:0]				write_data;
reg[3:0]				CNT_NUM;
 
assign				HZ_en=(start_en||single_w==0)?sda_out:1'bz; //只有当start_en与single_w
 
always(posedge clk_double or negedge ) begin
	state<=0;HZ_en<=0;
	case(state)
				4'd0:begin//主设备在此状态等待发送指令到来，当指令allow到来(即为0）时，状态跳转为1
					if(start_en!=YES)begin
						state<=0;HZ_en<=0;
					end
					else begin
						state<=1;dev_addr<=number[31:24];reg_addr_H<=number[23:16];reg_addr_L<=number[15:8];write_data<=number[7:0];
					end
			  end
				
			4'd1:begin
				if(I2C_SCL==1) begin sda_out<=0;end//当SCL为高电平时，SDA拉低，从设备会视为主设备发出数据的开始信号。
				else begin sda_out<=sda_out;end
			end
			4'd2:begin//开始通过串口sda发送数据，D7S从设备地址为0X55(01010101)，详细寄存器地址为16位二进制地址，目前还不清楚要取哪一个寄存器
				if(CNT_NUM!=8)begin
					if(I2C_SCL==0)begin
						sda_out<=dev_addr[7-CNT_NUM];
						CNT_NUM<=CNT_NUM+1;
					end
					else begin
						sda_out<=sda_out;
					end
				end
				else begin
					CNT_NUM<=0;
					HZ_en<=1;
					state<=state+1;
				end
			end
			4'd3:begin//查询应答信号，当应答信号为0，说明从设备接收完信息，可以继续发送16位寄存器地址
				if(sda_out==0)begin 
					state<=state+1; 
					HZ_en<=0;
				end
				else begin
					state<=state;
				end
			end
			4'd4:begin//发送16位寄存器地址高八位
				if(CNT_NUM!=8)begin
					if(I2C_SCL==0)begin
						sda_out<=reg_addr_H[7-CNT_NUM];
						CNT_NUM<=CNT_NUM+1;
					end
					else begin
						HZ_en<=1;					
					end
				end
				else begin
					CNT_NUM<=0;
					HZ_en<=1;
					state<=state+1;
				end
			end
			4'd5:begin//查询应答信号，当应答信号为0，说明从设备接收完信息，可以继续发送16位寄存器地址低八位
				if(sda==0)begin 
					state<=state+1; 
					HZ_en<=0;
				end
				else begin
					state<=state;
				end
			end
			
			4'd6:begin//发送16位寄存器地址低八位
				if(CNT_NUM!=8)begin
					if(I2C_SCL==0)begin
						sda_out<=reg_addr_L[7-CNT_NUM];
						CNT_NUM<=CNT_NUM+1;
					end
					else begin
						HZ_en<=1;					
					end
				end
				else begin
					HZ_en<=1;
					state<=state+1;
					CNT_NUM<=0;
				end
			end
			
			4‘d7:begin//查询应答信号，当应答信号为0，说明从设备接收完信息，可以继续发送指令数据
				if(sda==0)begin 
					state<=state+1; 
					HZ_en<=0;
				end
				else begin
					state<=state;
				end
			end
	endcase
	
				4'd8:begin//发送指令数据
				if(CNT_NUM!=8)begin
					if(I2C_SCL==0)begin
						sda_out<=write_data[7-CNT_NUM];
						CNT_NUM<=CNT_NUM+1;
					end
					else begin
						HZ_en<=1;					
					end
				end
				else begin
					HZ_en<=0;
					state<=state+1;
					CNT_NUM<=0;
				end
			end
			
			4'd9:begin//发送结束指令
				if(I2C_SCL==1) begin 
					if(CNT_NUM!=1)begin
						sda_out<=1;state<=0;
					end
					else begin
						CNT_NUM<=CNT_NUM+1;
					end
				end//当SCL为高电平时，SDA拉高，从设备会视为主设备发出数据的结束信号。
				else begin sda_out<=sda_out;end
			end
end
endmodule